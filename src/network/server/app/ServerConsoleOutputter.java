package network.server.app;

import network.server.actions.Action;
import network.server.actions.ActionOfferedListener;
import network.server.actions.Actions;

/** While no gui is available. */
public class ServerConsoleOutputter implements ActionOfferedListener{

	@Override
	public void actionOffered() {
		while(Actions.hasActions()) {
			Action a = Actions.take();
			System.out.println("[" + a.getType().toString() + " => t :: 0x" + Long.toHexString(a.getTime()) + " => aId :: 0x" + Integer.toHexString(a.getId()) + "]" +
					" " + a.getMessage());
		}
	}
}
