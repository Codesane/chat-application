package network.client.app;

import network.client.util.ClientContext;
import network.client.util.Users;
import network.client.util.ClientContext.ServerStatus;

import org.jboss.netty.channel.ConnectTimeoutException;

import exceptions.ServerOfflineException;
import gui.client.app.ClientGuiManager;

public class ClientLauncher {
	
	private Client client;
	
	private ClientGuiManager gui;
	
	public ClientLauncher(String host, int port) {
		
		client = new Client();
		
		gui = new ClientGuiManager();
		ClientContext.updateListener = gui;
		Users.setEventUpdateListener(gui);
		ClientContext.messageListener = gui;
		gui.displayLoginWindow();
		
		try {
			client.connect(host, port);
		} catch (ServerOfflineException soeMsg) {
			soeMsg.printStackTrace();
		} catch (ConnectTimeoutException cteMsg) {
			cteMsg.printStackTrace();
		}
		if(client.isConnected()) {
			gui.getLoginWindow().setServerStatusInfo(ServerStatus.ONLINE);
		} else {
			gui.getLoginWindow().setServerStatusInfo(ServerStatus.OFFLINE);
		}
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				client.shutdown();
			}
		});
	}
	
	public static void main(String[] args) {
		new ClientLauncher(args[0], Integer.parseInt(args[1]));
	}
}
